SAMBA SETUP SCRIPT 

This bash script will setup Samba file server for you.

Only prerequisite is Samba File server

USAGE: sudo ./sambaScript [-d option] [-p option] [-u option] [-b option] [-r option]

OPTIONS:
    -d creates specified directory in $HOME/Share
    -p sets permission for created shared directory. DEFAULT PERMISSIONS ARE
    644
    -u creates new samba user
    -b sets directory browseable. browseable = yes
    -r sets created directory read only. DEFAULT IS NO.
    -h prints this help message



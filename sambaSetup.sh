#!/bin/bash

set -e

function usage {
  echo "Run as root"
  echo "This script requires arguments"
  echo "USAGE: $0 [-d option] [-p option] [-u option] [-b option] [-r option]
  [-h option] "
  echo "OPTIONS:
    -d creates specified directory in $HOME/Share
    -p sets permission for created shared directory. DEFAULT PERMISSIONS ARE
    644
    -u creates new samba user
    -b sets directory browseable. browseable = yes
    -r sets created directory read only. DEFAULT IS NO.
    -h prints this help message
  "
}

function restart {
  service smbd restart || systemctl smbd.service restart
}

path="$HOME/Share"
public="$path/public"

if [ $1 = "-h" ]; then
  usage
  exit 1;
fi

if ! [ -d $path ]; then
  mkdir $path
else 
  echo >&2 "Directory $path already exists"
fi

if [[ $EUID -ne 0 ]]; then
	echo "root"
  usage
	exit 1
fi 

if [[ $# -eq 0 ]]; then
	echo "This script requires arguments"; usage
	exit 1
fi
input=false
echo "Checking if samba server is installed on your machine"
sleep 1;
command -v samba >/dev/null 2>&1 && echo "samba is installed" || { 
  echo >&2 "Samba is not installed on your system."  
  while !(( input )); do
    read -p "Do you want to install samba? [Y/n]" answer
    case $yn in
			"Y" | "y" | "" | "YES" | "Yes") apt install samba -y; break;;
			"N" | "n" | "NO" | "No") exit;;
			* ) echo "Invalid input!";;
		esac    
  done
}

restart

dir=""
permission=644
user=""
browseable="no"
ro="no"

while getopts d:p:u:bhr flag; do
  case $flag in 
    d) dir=$OPTARG;;
    p) permission=$OPTARG;;
    u) user=$OPTARG;
       echo "pass for user $user";
       smbpasswd -a $user;;
    b) browseable="yes";;
    r) ro="yes";;
    h) usage; exit 1 ;;
    *) echo "something wrong, invalid flag: $flag";
      usage; exit 1;;
  esac
done

shift $(($OPTIND - 1))

if ! [ -d $public ]; then
  mkdir $public 
  chmod 777 $public
  echo "
    [public files]
     comment = public files
     path = $path
     browseable = yes
     read only = no
     guest ok = yes
  " >> /etc/samba/smb.conf
fi

if ! [ -d $path/$dir ]; then
  mkdir $path/$dir
  chmod $permission $path/$dir
  echo "
    [$dir files]]
     comment = $dir files
     path = $path/$dir
     browseable = $browseable
     read only = $ro
  " >> /etc/samba/smb.conf
else 
  echo "Directory $dir already exist"
fi

restart
